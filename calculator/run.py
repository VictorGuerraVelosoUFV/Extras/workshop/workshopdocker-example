from datetime import datetime
from http import HTTPStatus

import requests
from flask import Flask, request, abort
from flask_restplus import Resource, Api, Namespace
from pymongo import MongoClient
import os
from flask_monitoringdashboard import bind as Dashboard

app = Flask(__name__)

client = MongoClient(f"mongodb://{os.getenv('MONGO_URI', 'calculator-mongo')}:27017/")
db = client.calculadora

Dashboard(app)

api = Api(app)

ns = Namespace(name="1.0", description="API version 1.0", path="/api")

api.add_namespace(ns)


@ns.route("/add")
class Addition(Resource):
    def get(self):
        params = []
        for k, v in request.args.to_dict().items():
            params.append(f"{k}={v}")
        db.addition.insert_one({"when": str(datetime.now()), "params": params})
        return requests.get(f"http://addition:5000/add/?{'&'.join(params)}").json()


@ns.route("/multiply")
class Multiplication(Resource):
    def get(self):
        params = []
        for k, v in request.args.to_dict().items():
            params.append(f"{k}={v}")
        db.multiplication.insert_one({"when": str(datetime.now()), "params": params})
        return requests.get(f"http://multiplication:5000/multiply/?{'&'.join(params)}").json()


@ns.route("/subtract")
class Subtraction(Resource):
    def get(self):
        params = []
        for k, v in request.args.to_dict().items():
            params.append(f"{k}={v}")
        db.subtraction.insert_one({"when": str(datetime.now()), "params": params})
        return requests.get(f"http://subtraction:5000/subtract/?{'&'.join(params)}").json()


@ns.route("/divide")
class Division(Resource):
    def get(self):
        params = []
        for k, v in request.args.to_dict().items():
            params.append(f"{k}={v}")
        db.division.insert_one({"when": str(datetime.now()), "params": params})
        return requests.get(f"http://division:5000/divide/?{'&'.join(params)}").json()


@ns.route("/log/<op>")
class Log(Resource):
    def get(self, op):
        collection = None
        if op == "division":
            collection = db.division
        elif op == "multiplication":
            collection = db.multiplication
        elif op == "addition":
            collection = db.addition
        elif op == "subtraction":
            collection = db.subtraction
        if collection is None:
            abort(HTTPStatus.BAD_REQUEST)
        else:
            res = []
            for i in collection.find({}):
                i["_id"] = str(i["_id"])
                res.append(i)
            return res


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
