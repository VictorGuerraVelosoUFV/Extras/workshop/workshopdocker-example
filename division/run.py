from flask import Flask, request
from flask_restplus import Resource, Api, Namespace
import os
from functools import reduce
import operator

app = Flask(__name__)

api = Api(app)

ns = Namespace(name="Division", description="Division methods", path="/divide")

api.add_namespace(ns)


@ns.route("/")
class Division(Resource):
    def get(self):
        return reduce(operator.truediv, [int(v) for v in request.args.to_dict().values()] + [1])


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
