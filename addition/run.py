from flask import Flask, request
from flask_restplus import Resource, Api, Namespace
import os

app = Flask(__name__)

api = Api(app)

ns = Namespace(name="Addition", description="Addition methods", path="/add")

api.add_namespace(ns)


@ns.route("/")
class Addition(Resource):
    def get(self):
        return sum([int(v) for v in request.args.to_dict().values()])


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
