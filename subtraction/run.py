from flask import Flask, request
from flask_restplus import Resource, Api, Namespace
import os
from functools import reduce
import operator

app = Flask(__name__)

api = Api(app)

ns = Namespace(name="Subtraction", description="Subtraction methods", path="/subtract")

api.add_namespace(ns)

@ns.route("/")
class Subtraction(Resource):
    def get(self):
        return reduce(operator.sub, [int(v) for v in request.args.to_dict().values()] + [0])

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
